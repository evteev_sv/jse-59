package com.nlmk;

import com.nlmk.config.HibernateConfig;
import com.nlmk.entity.Bus;
import com.nlmk.entity.ElectricCar;
import com.nlmk.entity.SportsCar;
import com.nlmk.entity.Truck;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Application {


    public static void main(String[] args) {
        Bus bus = Bus.builder().maxPassengers(52).brand("LIAZ").model("11-01").year(LocalDateTime.now()).build();
        Bus bus1 = Bus.builder().maxPassengers(43).brand("IKARUS").model("542")
                .year(LocalDateTime.parse("05.04.1986 08:00:14", DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss")))
                .build();
        Bus bus2 = Bus.builder().maxPassengers(42).brand("ПАЗ").model("1701").year(LocalDateTime.now()).build();

        Truck truck = Truck.builder().carrying(42).brand("KAMAZ").model("4310").year(LocalDateTime.now()).build();
        Truck truck1 = Truck.builder().carrying(16).brand("ЗИЛ").model("151").year(LocalDateTime.now()).build();
        Truck truck2 = Truck.builder().carrying(21).brand("Урал").model("5420").year(LocalDateTime.now()).build();

        ElectricCar electricCar = ElectricCar.builder().maxDistance(450).brand("Toyota").model("4458").year(LocalDateTime.now()).build();
        ElectricCar electricCar1 = ElectricCar.builder().maxDistance(300).brand("Tesla").model("new").year(LocalDateTime.now()).build();
        ElectricCar electricCar2 = ElectricCar.builder().maxDistance(600).brand("Lada").model("Granta").year(LocalDateTime.now()).build();

        SportsCar sportsCar = SportsCar.builder().acceleration(9).horsePower(400).brand("NONAME").model("MODEL").year(LocalDateTime.now()).build();
        SportsCar sportsCar1 = SportsCar.builder().acceleration(19).horsePower(300).brand("BMW").model("320").year(LocalDateTime.now()).build();
        SportsCar sportsCar2 = SportsCar.builder().acceleration(20).horsePower(100).brand("AUDI").model("Q5").year(LocalDateTime.now()).build();

        SessionFactory sessionFactory = HibernateConfig.getSessionFactory();
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.persist(bus);
            session.persist(bus1);
            session.persist(bus2);

            session.persist(truck);
            session.persist(truck1);
            session.persist(truck2);

            session.persist(electricCar);
            session.persist(electricCar1);
            session.persist(electricCar2);

            session.persist(sportsCar);
            session.persist(sportsCar1);
            session.persist(sportsCar2);
            transaction.commit();

        } catch (Exception e) {
            if (transaction != null)
                transaction.rollback();
            throw e;
        }

        try (Session session = sessionFactory.openSession()) {
            Query<Bus> busQuery = session.createQuery("from Bus b");
            System.out.println(busQuery.list());
            Query<Truck> truckQuery = session.createQuery("from Truck t");
            System.out.println(truckQuery.list());
            Query<ElectricCar> electricCarQuery = session.createQuery("from ElectricCar e");
            System.out.println(electricCarQuery.list());
            Query<SportsCar> sportsCarQuery = session.createQuery("from SportsCar s");
            System.out.println(sportsCarQuery.list());
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        }

    }
}
