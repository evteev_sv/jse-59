package com.nlmk.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity
@Table(name = "sports_car")
public class SportsCar extends Vehicle {

    @Column(name = "horse_power", nullable = false)
    private Integer horsePower;
    @Column(name = "acceleration", nullable = false)
    private Integer acceleration;

    @Override
    public String toString() {
        return "SportsCar{" +
                "horsePower=" + horsePower +
                ", acceleration=" + acceleration +
                "} " + super.toString();
    }
}
