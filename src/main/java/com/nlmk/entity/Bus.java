package com.nlmk.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity
@Table(name = "bus")
public class Bus extends Vehicle {

    @Column(name = "max_passengers", nullable = false)
    private Integer maxPassengers;

    @Override
    public String toString() {
        return "Bus{" +
                "maxPassengers=" + maxPassengers +
                "} " + super.toString();
    }
}
