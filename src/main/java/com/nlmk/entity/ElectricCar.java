package com.nlmk.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity
@Table(name = "electric_car")
public class ElectricCar extends Vehicle {

    @Column(name = "max_distance", nullable = false)
    private Integer maxDistance;

    @Override
    public String toString() {
        return "ElectricCar{" +
                "maxDistance=" + maxDistance +
                "} " + super.toString();
    }
}
