package com.nlmk.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity
@Table(name = "truck")
public class Truck extends Vehicle {

    @Column(name = "carrying", nullable = false)
    private Integer carrying;

    @Override
    public String toString() {
        return "Truck{" +
                "carrying=" + carrying +
                "} " + super.toString();
    }
}
