package com.nlmk.config;

import com.nlmk.entity.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateConfig {

    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory != null) return sessionFactory;

        Configuration cfg = new Configuration();
        cfg.addAnnotatedClass(Vehicle.class);
        cfg.addAnnotatedClass(Bus.class);
        cfg.addAnnotatedClass(Bus.class);
        cfg.addAnnotatedClass(Truck.class);
        cfg.addAnnotatedClass(ElectricCar.class);
        cfg.addAnnotatedClass(SportsCar.class);

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(cfg.getProperties())
                .build();

        sessionFactory = cfg.buildSessionFactory(serviceRegistry);

        return sessionFactory;
    }
}
